package br.com.tlf.fasttrack.productscatalog.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomHttpResponseDto {

	private Integer status_code;
	private String message;

}
