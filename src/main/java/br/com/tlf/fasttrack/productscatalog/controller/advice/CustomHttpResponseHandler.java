package br.com.tlf.fasttrack.productscatalog.controller.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

import br.com.tlf.fasttrack.productscatalog.controller.dto.CustomHttpResponseDto;


@RestControllerAdvice
public class CustomHttpResponseHandler {

	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public CustomHttpResponseDto handleMethodArgumentNotValidException(MethodArgumentNotValidException exception) {

		return new CustomHttpResponseDto(HttpStatus.BAD_REQUEST.value(), exception.getMessage());
	}
	
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	@ExceptionHandler(ResponseStatusException.class)
	public CustomHttpResponseDto handleNotFound(ResponseStatusException e) {

		return new CustomHttpResponseDto(HttpStatus.NOT_FOUND.value(), e.getReason());
	}

	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	public CustomHttpResponseDto handleException(Exception exception) {

		return new CustomHttpResponseDto(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getMessage());
	}
}
