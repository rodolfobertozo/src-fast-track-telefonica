package br.com.tlf.fasttrack.productscatalog.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.tlf.fasttrack.productscatalog.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

	List<Product> findByName(String q);

	@Query("SELECT p FROM Product p WHERE (lower(p.name) LIKE lower(concat('%', :q,'%')) OR lower(p.description) LIKE lower(concat('%', :q,'%'))) AND p.price >= :min_price AND p.price <= :max_price")
	List<Product> search(String q, BigDecimal min_price, BigDecimal max_price);

}
