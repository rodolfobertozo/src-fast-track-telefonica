package br.com.tlf.fasttrack.productscatalog.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.tlf.fasttrack.productscatalog.controller.dto.ProductDto;
import br.com.tlf.fasttrack.productscatalog.model.Product;
import br.com.tlf.fasttrack.productscatalog.repository.ProductRepository;

@Service
public class ProductsService {

	@Autowired
	private ProductRepository repository;

	@Autowired
	private ModelMapper modelMapper;

	public ProductsService() {
	}

	private ProductDto convertToDto(Product product) {
		ProductDto productDto = modelMapper.map(product, ProductDto.class);
		return productDto;
	}

	private Product convertDtoToModel(ProductDto productDto) {
		Product product = modelMapper.map(productDto, Product.class);
		return product;
	}

	@Transactional
	public ProductDto create(ProductDto productDto, UriComponentsBuilder uriBuilder) {

		Product product = repository.save(convertDtoToModel(productDto));
		productDto = convertToDto(product);

		return productDto;
	}

	@Transactional
	public ProductDto update(ProductDto productDto, Long id) {

		Optional<Product> optional = repository.findById(id);

		if (optional.isPresent()) {
			Product product = optional.get();
			product.setName(productDto.getName());
			product.setDescription(productDto.getDescription());
			product.setPrice(productDto.getPrice());

			productDto = convertToDto(product);

			return productDto;
		}

		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product With id " + id.toString() + " Not Found");
	}

	public List<ProductDto> getProducts() {

		List<Product> products = repository.findAll();

		return products.stream().map(this::convertToDto).collect(Collectors.toList());
	}

	public ProductDto findById(Long id) {

		Optional<Product> product = repository.findById(id);

		if (product.isPresent()) {
			return convertToDto(product.get());
		}

		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product With id " + id.toString() + " Not Found");
	}

	public List<ProductDto> search(String q, BigDecimal min_price, BigDecimal max_price) {

		List<Product> products = repository.search(q, min_price, max_price);

		return products.stream().map(this::convertToDto).collect(Collectors.toList());
	}

	@Transactional
	public ProductDto remove(Long id) {

		Optional<Product> product = repository.findById(id);
		if (product.isPresent()) {
			repository.deleteById(id);
			return convertToDto(product.get());
		}

		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product With id " + id.toString() + " Not Found");
	}
}