package br.com.tlf.fasttrack.productscatalog.controller.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {

	private Long id;
	@NotBlank
	private String name;
	@NotBlank
	private String description;
	@NotNull
	@Positive
	private BigDecimal price;

}
