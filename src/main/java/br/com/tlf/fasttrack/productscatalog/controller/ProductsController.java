package br.com.tlf.fasttrack.productscatalog.controller;

import java.math.BigDecimal;
import java.net.URI;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.tlf.fasttrack.productscatalog.controller.dto.ProductDto;
import br.com.tlf.fasttrack.productscatalog.service.ProductsService;

/**
 * @author A0088691
 *
 */
@RestController
@RequestMapping("/products")
public class ProductsController {

	@Autowired
	private ProductsService service;

	@PostMapping
	@Transactional
	public ResponseEntity<ProductDto> create(@RequestBody @Valid ProductDto productDto,
			UriComponentsBuilder uriBuilder) {

		productDto = service.create(productDto, uriBuilder);
		URI uri = uriBuilder.path("/products/{id}").buildAndExpand(productDto.getId()).toUri();
		return ResponseEntity.created(uri).body(productDto);
	}

	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<ProductDto> update(@PathVariable Long id, @RequestBody @Valid ProductDto product) {
		ProductDto productDto = service.update(product, id);
		return ResponseEntity.ok(productDto);
	}

	@GetMapping
	public ResponseEntity<List<ProductDto>> getProducts() {
		List<ProductDto> products = service.getProducts();
		return ResponseEntity.ok(products);
	}

	@GetMapping("/{id}")
	public ResponseEntity<ProductDto> findById(@PathVariable Long id) {
		ProductDto productDto = service.findById(id);
		return ResponseEntity.ok(productDto);
	}

	@GetMapping("/search")
	public ResponseEntity<List<ProductDto>> search(@RequestParam(defaultValue = "") String q,
			@RequestParam(defaultValue = "0.0") BigDecimal min_price,
			@RequestParam(defaultValue = "100000.00") BigDecimal max_price) {
		return ResponseEntity.ok(service.search(q, min_price, max_price));
	}

	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<ProductDto> remove(@PathVariable Long id) {
		return ResponseEntity.ok(service.remove(id));
	}

}