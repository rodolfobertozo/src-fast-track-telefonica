package br.com.tlf.fasttrack.productscatalog;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.URI;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductsControllerTest {
	
	@Autowired
	private MockMvc mock;
	
	@Test
	public void shouldReturnHttp200WhenGetAllProducts() throws Exception {
		
		URI uri = new URI("/products");

		mock.perform(get(uri))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON));
	}
	
	@Test
	public void shouldReturnHttp200WhenGetSomeProductById() throws Exception {
		
		URI uri = new URI("/products/1");

		mock.perform(get(uri))
		.andExpect(status().is(200));
	}
	
	@Test
	public void shouldReturnHttp404WhenGetSomeProductAndIdNotFound() throws Exception {
		
		URI uri = new URI("/products/333");

		mock.perform(get(uri))
		.andExpect(status().is(404));
	}
	
	@Test
	public void shouldReturnHttp201WhenCreatingAProduct() throws Exception {
		
		URI uri = new URI("/products");
		String json = "{\"name\":\"Mouse Logitec\",\"description\":\"Mouse Logitec MS Anywhere 2s\",\"price\":255}";

		mock.perform(post(uri).content(json).contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().is(201));
	}
	
	@Test
	public void shouldReturnHttp400WhenCreatingAProductWhithNegativePrice() throws Exception {
		
		URI uri = new URI("/products");
		String json = "{\"name\":\"Mouse Logitec\",\"description\":\"Mouse Logitec MS Anywhere 2s\",\"price\":-255}";

		mock.perform(post(uri).content(json).contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().is(400));
	}
	
	@Test
	public void shouldReturnHttp400WhenCreatingAProductWithBlankName() throws Exception {
		
		URI uri = new URI("/products");
		String json = "{\"name\":\"\",\"description\":\"Mouse Logitec MS Anywhere 2s\",\"price\":255}";

		mock.perform(post(uri).content(json).contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().is(400));
	}
	
	@Test
	public void shouldReturnHttp400WhenCreatingAProductWithBlankDescription() throws Exception {
		
		URI uri = new URI("/products");
		String json = "{\"name\":\"Mouse Logitec\",\"description\":\"\",\"price\":255}";

		mock.perform(post(uri).content(json).contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().is(400));
	}
	
	@Test
	public void shouldReturnHttp200WhenUpdatingSomeProduct() throws Exception {
		
		URI uri = new URI("/products/1");
		String json = "{\"name\":\"Mouse Logitec\",\"description\":\"Mouse Logitec MS Anywhere 2s\",\"price\":233}";

		mock.perform(put(uri).content(json).contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().is(200));
	}
	
	@Test
	public void shouldReturnHttp400WhenUpdatingAProductWhithNegativePrice() throws Exception {
		
		URI uri = new URI("/products");
		String json = "{\"name\":\"Mouse Logitec\",\"description\":\"Mouse Logitec MS Anywhere 2s\",\"price\":-255}";

		mock.perform(post(uri).content(json).contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().is(400));
	}
	
	@Test
	public void shouldReturnHttp400WhenUpdatingAProductWithBlankName() throws Exception {
		
		URI uri = new URI("/products");
		String json = "{\"name\":\"\",\"description\":\"Mouse Logitec MS Anywhere 2s\",\"price\":255}";

		mock.perform(post(uri).content(json).contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().is(400));
	}
	
	@Test
	public void shouldReturnHttp400WhenUpdatingAProductWithBlankDescription() throws Exception {
		
		URI uri = new URI("/products");
		String json = "{\"name\":\"Mouse Logitec\",\"description\":\"\",\"price\":255}";

		mock.perform(post(uri).content(json).contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().is(400));
	}
	
	@Test
	public void shouldReturnHttp404WhenUpdatingAnIdNotFound() throws Exception {
		
		URI uri = new URI("/products/99");
		String json = "{\"name\":\"Mouse Logitec\",\"description\":\"Mouse Logitec MS Anywhere 2s\",\"price\":233}";

		mock.perform(put(uri).content(json).contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().is(404));
	}
	
	@Test
	public void shouldReturnHttp200WhenDeletingSomeProduct() throws Exception {
		
		URI uri = new URI("/products/3");

		mock.perform(delete(uri))
		.andExpect(status().is(200));
	}
	
	@Test
	public void shouldReturnHttp404WhenDeletingSomeProductAndIdNotFound() throws Exception {
		
		URI uri = new URI("/products/333");

		mock.perform(delete(uri))
		.andExpect(status().is(404));
	}
}
